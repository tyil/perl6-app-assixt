#! /usr/bin/env perl6

use v6.c;

use Test;
use Test::Output;

BEGIN plan :skip-all<set AUTHOR_TESTING=1 to run bin tests> unless %*ENV<AUTHOR_TESTING>;

use Config;
use File::Temp;
use File::Which;

use App::Assixt;
use App::Assixt::Commands::Dist;
use App::Assixt::Config;
use App::Assixt::Test;

plan 3;

$App::Assixt::silent = True;

skip-rest "'tar' is not available" and exit unless which("tar");

my $assixt = $*CWD;
my IO::Path $module = create-test-module("Local::Test::Dist", tempdir.IO);
my IO::Path $storage = tempdir.IO;

my Config $config = get-config(:!user-config).read: %(
	assixt => %(
		distdir => $storage.absolute,
	),
);

subtest "Create dist with normal config", {
	plan 2;

	ok App::Assixt::Commands::Dist.run(
		$module,
		:$config,
	), "Dist runs correctly";

	ok "$config<assixt><distdir>/Local-Test-Dist-0.0.0.tar.gz".IO.e, "Tarball exists";
};

subtest "--output-dir overrides config-set output-dir", {
	plan 2;

	my IO::Path $output-dir = tempdir.IO;

	my Config $local-config = $config.clone.read: %(
		runtime => %(
			output-dir => $output-dir.absolute,
		),
	);

	ok App::Assixt::Commands::Dist.run(
		$module,
		config => $local-config,
	), "assixt dist --output-dir=$output-dir";

	ok $output-dir.add("Local-Test-Dist-0.0.0.tar.gz").e, "Tarball exists";
};

subtest "--output-dir set to a path with spaces", {
	plan 2;

	my IO::Path $output-dir = tempdir.IO.add("o u t p u t");

	my Config $local-config = $config.clone.read: %(
		runtime => %(
			output-dir => $output-dir.absolute,
		),
	);

	ok App::Assixt::Commands::Dist.run(
		$module,
		config => $local-config,
	), "assixt dist --output-dir='{$output-dir.absolute}'";

	ok "$output-dir/Local-Test-Dist-0.0.0.tar.gz".IO.e, "Tarball exists";
}

# vim: ft=perl6 noet
