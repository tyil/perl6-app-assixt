FROM registry.gitlab.com/tyil/docker-perl6:alpine-dev-latest AS build

WORKDIR /root

RUN apk add --no-cache curl git alpine-sdk

COPY META6.json META6.json
COPY bin bin
COPY lib lib
COPY resources resources

RUN zef install --/test .

FROM registry.gitlab.com/tyil/docker-perl6:alpine-latest

WORKDIR /tmp

ENV PATH=/usr/local/share/perl6/site/bin:$PATH

COPY --from=build /usr/local /usr/local

CMD [ "sh" ]
